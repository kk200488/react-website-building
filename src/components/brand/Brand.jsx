import React from 'react'
import './brand.css';
import epfo1 from '../../assets/slide/epfo1.jpeg'
import epfo2 from '../../assets/slide/epfo2.jpeg'
import epfo3 from '../../assets/slide/epfo3.png'
import esic1 from '../../assets/slide/esic1.jpeg'



const Brand = () => {
  return (
    <div>
      <div className='velavan__about'>
      <div className='velavan__about_heading'>
        <h1>About Us</h1>
      </div>
      <div className='velavan__about_text'>

        <p>
        We provide best PF ESI Consultancy service. We do PF ESI Registration, PF transfer and withdraw, PF ESI Monthly filing
        We have 30 years of Experiene in ESI , PF and Labour Laws. We are doing the business for the second generation. 
        </p>
      </div>

      <div className='velavan__image'>
        <div className='velavan__image_list'>
          <img src={epfo1} alt=''></img>
          <img src={epfo2} alt=''></img>
          <img src={epfo3} alt=''></img>
          <img src={esic1} alt=''></img>
          <img src={epfo1} alt=''></img>
          <img src={epfo2} alt=''></img>
          <img src={epfo3} alt=''></img>
          <img src={esic1} alt=''></img>
        </div>
        </div>

      </div>
    </div>
  )
}

export default Brand