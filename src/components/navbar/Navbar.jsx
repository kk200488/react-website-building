import React, { useState } from 'react';
import { RiMenu3Line, RiCloseLine } from 'react-icons/ri';
// import logo from '../../logo.svg';
// import logo from '../../velavan.png';
import logo_first from './logo1.svg';
import logo_last from './logo2.svg';

import fav from './fav.svg';
import {Link} from 'react-router-dom';


import './navbar.css';

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);

  const [color,setColor] = useState(false);
  const changeColor = () => {
    if (window.scrollY >= 90) {
      setColor(true)
    } else {
      setColor(false)
    }
  }

  window.addEventListener('scroll',changeColor)

  return (
    <div className={color ? 'velavan__navbar velevan__navbar_black': 'velavan__navbar velevan__navbar_blue'}>
      <div className="velavan__navbar-links">
        <div className="velavan__navbar-links_logo">
        <img src={fav} alt='' width={'7vh'}/> 
        <div className='velavan'>
        <div className='velavan_first'>
        </div>
        <img src={logo_first} alt=''/> 
        <div className='velavan_second'>
        </div>

        <img src={logo_last} alt=''/> 

        </div>
        </div>
        <div className="velavan__navbar-links_container">
          {/* <p><a href="#home">Home</a></p>
          <p><a href="#wvelavan">Services</a></p>
          <p><a href="#possibility">ESIC</a></p>
          <p><a href="#pfbenefits">PF </a></p>
          <p><a href="#blog">Article</a></p> */}
          <p><Link to="/react-website-building/">Home</Link></p>
          <p><Link to="/react-website-building/wgpt3">Services</Link></p>
          <p><Link to="/react-website-building/esic">ESIC</Link></p>
          <p><Link to="/react-website-building/pf"> PF   </Link></p>
          <p><Link to="/react-website-building/blog">Blog </Link></p>
        </div>
      </div>
      {/* <div className="velavan__navbar-sign">
        <p>Sign in</p>
        <button type="button">Sign up</button>
      </div> */}
      <div className="velavan__navbar-menu">
        {toggleMenu
          ? <RiCloseLine color="#fff" size={20} onClick={() => setToggleMenu(false)} />
          : <RiMenu3Line color="#fff" size={20} onClick={() => setToggleMenu(true)} />}
        {toggleMenu && (
        <div className="velavan__navbar-menu_container scale-up-center">
          <div className="velavan__navbar-menu_container-links">
            {/* <p><a href="#home">Home</a></p>
            <p><a href="#wvelavan">Services</a></p>
            <p><a href="#possibility">ESIC</a></p>
            <p><a href="#pfbenefits">PF</a></p>
            <p><a href="#blog">Article</a></p> */}
            <p><Link to="/react-website-building/">Home</Link></p>
            <p><Link to="/react-website-building/wgpt3">Services</Link></p>
            <p><Link to="/react-website-building/esic">ESIC</Link></p>
            <p><Link to="/react-website-building/pf">PF </Link></p>
            <p><Link to="/react-website-building/blog">Blog</Link></p>
          </div>
          {/* <div className="velavan__navbar-menu_container-links-sign">
            <p>Sign in</p>
            <button type="button">Sign up</button>
          </div> */}
        </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;