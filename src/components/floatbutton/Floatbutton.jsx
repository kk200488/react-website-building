import React from 'react'
import './floatbutton.css'
import {useEffect} from 'react';


const Floatbutton = () => {

    useEffect(() => {
      // 👇️ scroll to top on page load
      window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    }, []);

  return (
    <div>

  <div>
    <button onclick={() => {
          window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        }} className="scroll-to-top" title="Go to top">Top</button>
    </div>
  <div>

    <a href="https://api.whatsapp.com/send?phone=919003252117&text=Hello+Velavan+Consultancy!+I+am+looking+for+EPFO%2FESIC%2FDSC+services" class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
    </a>
    {/* <a href="tel:9003252117" class='float-next'>call</a> */}
  </div>
  </div>

  )
}

export default Floatbutton