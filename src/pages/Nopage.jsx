import React from 'react';

function NoPage() {
    return (
        <div className='nopage'>
            <h1>404 Error</h1>
        </div>
    );
}

export default NoPage;