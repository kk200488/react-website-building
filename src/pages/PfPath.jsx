import React from 'react';
import { Pfbenefits, Pfsection } from '../containers';

const PfPath = () => (
  <div>
    <Pfsection />
    <Pfbenefits />
  </div>
);

export default PfPath;