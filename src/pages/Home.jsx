// import React from 'react';
// import ReactDOM from 'react-dom/client';
// // import './index.css';
// // import App from './App';
// // import reportWebVitals from './reportWebVitals';

// // ReactDOM.render(<App />, document.getElementById('root'));
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//     <h1>this is the homepage</h1>
// );

// export default Home;

import React from 'react';
import { Brand} from '../components'
import {Features, WhatsGPT3} from '../containers'

function Home() {
    return (
        <div>
        <Brand />
        <Features />
        </div>
    );
}

export default Home;