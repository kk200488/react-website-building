import React from 'react';
import { EsicSection, Esicbenefits } from '../containers';

const EsicTab = () => (
  <div>
    <EsicSection />
    <Esicbenefits />
  </div>
);

export default EsicTab;