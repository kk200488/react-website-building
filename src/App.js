// import logo from './logo.svg';
import './App.css';

import {Footer, Possibility, Pfsection, Features, WhatsGPT3, Header, Esicbenefits, Pfbenefits} from './containers'
import {CTA, Brand, Navbar, Floatbutton} from './components'
// import { Route, Link} from 'react-router-dom'
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import {PfPath, EsicTab, Blogs, Nopage, Home} from './pages';

const App = () => {
  return (
    <div className='App'>
        <Navbar />


        <Routes>
          <Route  path='react-website-building/' element={<Home />} />
          <Route exact path='react-website-building/esic' element={<EsicTab />} />
          <Route exact path='react-website-building/pf' element={<PfPath />} />
          <Route exact path='react-website-building/blog' element={<Blogs />} />
          <Route exact path='*' element={<Nopage />} />

        </Routes>
        <Footer />
        <Floatbutton />
      {/* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"></link>
      <div  className='gradient__bg'>
        <Navbar />
        {/* <Header /> 
        <Floatbutton />
      </div>
      
      <Brand />
     <WhatsGPT3 /> 
      <Features />
      <Possibility />
      <Esicbenefits />
      <Pfsection />
      <Pfbenefits />

      <CTA />

      <Blog />
      <Footer />  */}
    </div>
  )
}

export default App;
