import React from 'react'
import './header.css';
import hand from '../../assets/hand-shake.jpg'

const Header = () => {
  return (
  <div class="velavan__header">
    <div class="velavan__header_row">
      <div class="velavan__header_column1">
        <img src={hand} loading="lazy" width="100"  alt="" class="image"/>        
      </div>
      <div class="velavan__header_column2">
        <h1  class="hero-heading">Velavan Consultancy</h1>
        <div class="hero-subheading">Your TRUST , OUR CARE</div>
      </div>
    </div>
  </div>

  )
}

export default Header

