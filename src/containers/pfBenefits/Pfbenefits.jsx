import React from 'react'
import education from '../../assets/pfbenefits/education.png'
import house from '../../assets/pfbenefits/house.png'
import wedding from '../../assets/pfbenefits/wedding.png'

import pension from '../../assets/pfbenefits/pension.png'
import moneybag from '../../assets/pfbenefits/money-bag.png'



import cash from '../../assets/esicbenefits/cash.png'
import doctor from '../../assets/esicbenefits/doctor.png'


import './pfbenefits.css'

const Pfbenefits = () => {
  return (
    <div className="velavan__pf" >
        <div className="velavan__pfbenefits"> 
        <div className="velavan__pfbenefits-content">
        {/* <p>Request Early Access to Get Started</p> */}
            <h3>The Employees' Provident Funds Scheme 1952 (EPF)</h3>
            <ul>
              <li>
                <p>Accumulation plus interest upon retirement, resignation , death.</p>
              </li>
              <li>
                <p>Partial withdrawals allowed for specific expenses such as house construction, higher education, marriage, illness etc.</p>
              </li>
              <div className="velavan__pfbenefits-image-benefits">
            <img src={house} alt="" width='50%'/>
            <img src={education} alt=""/>
            <img src={wedding} alt=""/>
            <img src={doctor} alt=""/>
          </div>
            </ul>
        </div>
            {/* <div className="velavan__pfbenefits-image-benefits">
              <p>Hospitalisation</p><p> & Outpatient</p>
              <img src={steth}  alt=""/>
            </div>
            <div className="velavan__pfbenefits-image-benefits">
              <p> Applicable</p>
              <img src={sandclock}  alt=""/>
              <p>From Day 1</p>
            </div> */}
        </div>



        <div className="velavan__pfbenefits">
        <div className="velavan__pfbenefits-content">
            {/* <p>Request Early Access to Get Started</p> */}
            <h3>The Employees' Pension Scheme 1995 (EPS)</h3>
            {/* <p>Sickness Benefit in the form of cash compensation</p>
            <p>at the rate of 70 per cent of wages is payable to insured workers during the </p>
            <p>periods of certified sickness for a maximum of 91 days in a year.</p>
            <p>In order to qualify for sickness benefit the Insured Person is required </p>
            <p>to contribute for 78 days in a contribution period of 6 months.</p> */}
              <ul>
                <li>
                  <p>Monthly Pension benefits for superannuation/ retirement, disability, survivor, widow (er), children.
</p> 
                </li>
                <li>        
                  <p>In order to qualify for sickness benefit the Insured Person is required to contribute for <em><b>78 days in a contribution period of 6 months.</b></em></p>
                </li>
                <div className="velavan__pfbenefits-image-benefits">
                <img src={cash}  alt=""/>
                <img src={pension}  alt=""/>
                </div>
              </ul>

        </div>

        </div>


        <div className="velavan__pfbenefits">
        <div className="velavan__pfbenefits-content">
        {/* <p>Request Early Access to Get Started</p> */}
            <h3>The Employees' Deposit Linked Insurance Scheme 1976 (EDLI)</h3>
            <ul>
              <li>
                <p>The benefit provided in case of death of an employee who was member of the scheme at the time of the death.
</p>
              </li>
              <li>
                <p>According to revised scheme payment of benefit amount to be 20 times of the wages or based on the deposit in the Provident Fund, which ever is less. With the increase in the wages ceiling from 6500/- to 15000/- from 01.09.2014, the maximum benefit amount has become 3 lakh and an additional 20% of the benefit amount calculated is also paid.</p>
              </li> 
              <div className="velavan__pfbenefits-image-benefits">
              <img src={moneybag}  alt=""/>
              </div>
            </ul>
        </div>
        </div>

    </div>
  )
}

export default Pfbenefits