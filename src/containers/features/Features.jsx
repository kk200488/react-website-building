import React from 'react'
import './features.css';
import pf_logo from '../../assets/official-epfo.jpg'
import esic_logo from '../../assets/official-esic.jpg'
import it_logo from '../../assets/official-it.jpg'

const Features = () => {
  return (
    <div className='velavan__services'>
    <div className='velavan__services_heading'>
      <h1 className="gradient__text">SERVICES OFFERED</h1>
    </div>
    <div className='velavan__services_sub-heading'>
      <h1>WE PROVIDE QUALITY SERVICES RELATED TO PF AND ESIC</h1>
    </div>
    <div className='velavan__services_body'>
      <div class="velavan__services_row">
        <div class="velavan__services_column-1">
          <img src={esic_logo} width='100px' alt=''/>
          <h3>ESIC SERVICES</h3>
          <p>ESIC card Service and guidance to get various ESIC benefits. Dispensary update , KYC Bank and Mobile Nummber update , link with UAN number</p>
        </div>
        <div class="velavan__services_column-2">
          <img src={pf_logo} width='100px' alt=''/>
          <h3>PF SERVICES</h3>
          <p>PF Registration , Monthly Filing , Salary and Attendance Register Maintenance for Employers.
PF Advance, Withdrawal , Transfer services for Employees.</p>
        </div>  
        <div class="velavan__services_column-3">
          <img src={it_logo} width='100px' height='100px' alt=''/>
          <h3>IT RETURNS, GST</h3>
          <p>IT Returns for Professionals Individuals, GST Services</p>
        </div>
      </div>
    </div>
    </div>
  )
}

export default Features