import React from 'react';
// import possibilityImage from '../../assets/possibility.png';
import possibilityImage from '../../assets/pf.png';
import './pfsection.css';

const Pfsection = () => (
  <div className="gpt3__possibility section__padding" id='pfbenefits'>
    <div className="gpt3__possibility-content">
      <h1 className="gradient__text">Employees Provident Fund (EPF) Schene <br /></h1>
      <ul>
        <li>Registration</li>
        <li>Link ESI insurance number with EPFO UAN</li>
        <li>All compilance regarding EPFO</li>
        <li>Help avail Employee benifits</li>
        <li>Issue Peachen card and guidance</li>
        <li>Guidance in cash benifits on Maternity, un-employment, sickness </li>
        <li>Issue Peachen card and guidance</li>
      </ul>
      {/* <p>Yet bed any for travelling assistance indulgence unpleasing. Not thoughts all exercise blessing. Indulgence way everything joy alteration boisterous the attachment. Party we years to order allow asked of.</p>
      <h4>Request Early Access to Get Started</h4> */}
    </div>
    <div className="gpt3__possibility-image">
      <img src={possibilityImage} alt="possibility" />
    </div>
  </div>
);

export default Pfsection;