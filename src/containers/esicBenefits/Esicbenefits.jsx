import React from 'react'
import sick from '../../assets/esicbenefits/sick.png'
import pregnant from '../../assets/esicbenefits/pregnant.png'
import diffrentlyabled from '../../assets/esicbenefits/diffrentlyabled.png'
import cash from '../../assets/esicbenefits/cash.png'
import steth from '../../assets/esicbenefits/stethoscope.png'
import sandclock from '../../assets/esicbenefits/calander.png'
import doctor from '../../assets/esicbenefits/doctor.png'


import './esicbenefits.css'

const Esicbenefits = () => {
  return (
    <div className="velavan__esic">

        <div className="velavan__esicbenefits start">
        <div className="velavan__esicbenefits_heading">
        </div>
          <div className="velavan__esicbenefits-image">
            <img src={doctor} alt=""/>
          </div>
        <div className="velavan__esicbenefits-content">
        <h3>Medical Benefit (self and family)</h3>

        {/* <p>Request Early Access to Get Started</p> */}
            <ul>
              <li>
                <p>Full medical care is provided to an Insured person and his family members</p>
              </li>
              <li>
                <p>From day one of entering insurable employment</p>
              </li>
            </ul>
        </div>
            <div className="velavan__esicbenefits-image-benefits">
              <p>Hospitalisation</p><p> & Outpatient</p>
              <img src={steth}  alt=""/>
            </div>
            <div className="velavan__esicbenefits-image-benefits">
              <p> Applicable</p>
              <img src={sandclock}  alt=""/>
              <p>From Day 1</p>
            </div>
        </div>



        <div className="velavan__esicbenefits middle">
        <div className="velavan__esicbenefits_heading">
        </div>
          <div className="velavan__esicbenefits-image">
            <img src={sick} alt=""/>
          </div>
        <div className="velavan__esicbenefits-content">
        <h3>Sickness Benefit</h3>

            {/* <p>Request Early Access to Get Started</p> */}
            {/* <p>Sickness Benefit in the form of cash compensation</p>
            <p>at the rate of 70 per cent of wages is payable to insured workers during the </p>
            <p>periods of certified sickness for a maximum of 91 days in a year.</p>
            <p>In order to qualify for sickness benefit the Insured Person is required </p>
            <p>to contribute for 78 days in a contribution period of 6 months.</p> */}
              <ul>
                <li>
                  <p>Sickness Benefit in the form of cash compensation
         at the rate of 70 per cent of wages is payable to insured workers during the
           periods of certified sickness for a maximum of 91 days in a year.   </p> 
                </li>
                <li>        
                  <p>In order to qualify for sickness benefit the Insured Person is required to contribute for <em><b>78 days in a contribution period of 6 months.</b></em></p>
                </li>
              </ul>

        </div>
              <div className="velavan__esicbenefits-image-benefits">
              <p>Cash Benefit</p>
              <img src={cash}  alt=""/>
              <p>70% wages </p><p> max 91 days/year</p>
            </div>
            <div className="velavan__esicbenefits-image-benefits">
              <p> Minimum Contribution</p>
              <img src={sandclock}  alt=""/>
              <p>78 days</p><p>in 6 months</p>
            </div>
        </div>


        <div className="velavan__esicbenefits middle">
        <div className="velavan__esicbenefits-image">
            <img src={pregnant} alt=""/>
        </div>
        <div className="velavan__esicbenefits-content">
        {/* <p>Request Early Access to Get Started</p> */}
            <h3>Maternity Benefit</h3>
            <ul>
              <li>
                <p>Full wage for 26 weeks <small>(extendable by further one month on medical advice)</small></p>
              </li>
              <li>
                <p>Contribution required for 78 days in 6 months</p>
              </li> 
            </ul>
        </div>
        <div className="velavan__esicbenefits-image-benefits">
              <p>Cash Benefit</p>
              <img src={cash}  alt=""/>
              <p>Full Wage </p><p>for 26 weeks</p>
        </div>
        <div className="velavan__esicbenefits-image-benefits">
              <p> Minimum Contribution</p>
              <img src={sandclock}  alt=""/>
              <p>78 days</p><p>in 6 months</p>
        </div>
        </div>


        <div className="velavan__esicbenefits end">
        <div className="velavan__esicbenefits-image">
            <img src={diffrentlyabled} alt=""/>
        </div>  
        <div className="velavan__esicbenefits-content">
        {/* <p>Request Early Access to Get Started</p> */}
            <h3>Disablement Benefit</h3>
            <ul>
              <li>            
                <p>Temporary Disablement Benefit at the rate of 90% of wage is payable so long as disability continues.</p>
              </li>
              <li>
                <p>From day one of entering insurable employment</p>
              </li>
            </ul>
        </div>

            <div className="velavan__esicbenefits-image-benefits">
              <p>Cash Benefit</p>
              <img src={cash}  alt=""/>
              <p>90% of wages</p>
            </div>
            <div className="velavan__esicbenefits-image-benefits">
              <p>Applicable</p>
              <img src={sandclock}  alt=""/>
              <p>From Day 1</p>
            </div>

        </div>
    </div>
  )
}

export default Esicbenefits